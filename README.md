来源：[http://www.eoeandroid.com/blog-23755-2661.html](http://www.eoeandroid.com/blog-23755-2661.html)

Android中如果应用出现异常，程序不做处理的话，通常会弹出“强行关闭”对话框，如下：

![http://bigcat.easymorse.com/wp-content/uploads/2011/05/image_thumb.png](http://bigcat.easymorse.com/wp-content/uploads/2011/05/image_thumb.png)