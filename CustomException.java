public class CustomException implements UncaughtExceptionHandler {
    //获取application 对象； 
    private Context mContext; 
    
    private Thread.UncaughtExceptionHandler defaultExceptionHandler; 
    //单例声明CustomException; 
    private static CustomException customException; 
    
    private CustomException(){        
    } 
    
    public static CustomException getInstance(){ 
        if(customException == null){ 
            customException = new CustomException(); 
        } 
        return customException; 
    } 
    
    @Override 
    public void uncaughtException(Thread thread, Throwable exception) { 
        // TODO Auto-generated method stub 
        if(defaultExceptionHandler != null){ 
            
            Log.e("tag", "exception >>>>>>>"+exception.getLocalizedMessage()); 
            //将异常抛出，则应用会弹出异常对话框.这里先注释掉 
         //   defaultExceptionHandler.uncaughtException(thread, exception); 
            
        } 
    } 
    
     public void init(Context context) {   
            mContext = context;   
            defaultExceptionHandler  = Thread.getDefaultUncaughtExceptionHandler();   
           Thread.setDefaultUncaughtExceptionHandler(this);  
          }
}

public void uncaughtException(Thread thread, Throwable exception) { 
        // TODO Auto-generated method stub 
        if(defaultExceptionHandler != null){ 
            
            Log.e("tag", "exception >>>>>>>"+exception.getLocalizedMessage()); 
            //将异常抛出，则应用会弹出异常对话框.这里先注释掉 
         //   defaultExceptionHandler.uncaughtException(thread, exception); 
            
        } 
    }